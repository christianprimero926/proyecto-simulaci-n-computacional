El problema de las n reinas consiste en colocar en un tablero de ajedrez n x n, n reinas sin
que estas se amenacen entre sí. Se tienen los siguientes datos:

1. Los robots llegan de acuerdo a una distribución uniforme entre 10 y 30 segundos.
2. El robot soluciona el problema de acuerdo con un algoritmo las vegas.
3. El profesor soluciona el problema de acuerdo con un algoritmo determinista.
4. Cuando va iniciar el juego, el robot y el humano seleccionan el tablero de acuerdo a una
distribución uniforme. Los posibles valores de n son 4,5,6,8,10,12 y 15.
5. El primero que solucione el problema gana.
6. La simulación tiene una duración de 8 horas

Lo interesante del ejercicio es cómo incluir el tiempo de ejecución de un algoritmo como
tiempo de servicio. Se busque que realice una simulación que ganancia obtiene usted después
de este ejercicio.