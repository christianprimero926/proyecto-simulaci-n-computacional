#       Nombre:                 Christian David Ospina Primero
#       Codigo:                 201556123
#       Codigo de programa:     3743
#
#       Nombre:                 Andres Camilo Pulgarin Nopia
#       Codigo:                 201556017
#       Codigo de programa:     3743
#
#       Nombre:                 Kevin Stewart Ramirez Gamba
#       Codigo:                 201556203
#       Codigo de programa:     3743
#---------------------------------------------------------------------------------------
#Escenario 1: 	en este punto se plantea que otro profesor llega ayudar al 
#				profesor actual
#---------------------------------------------------------------------------------------
import random
import simpy as sp                                      #importacion de modulos
import numpy as np
import time 
from timeit import timeit

import simulacion as sm                                 #Importacion de modulos propios

                                                        #Datos de la simulacion

SEMILLA = np.random.seed(0)                             #Semilla generador
LLEGADA_CLIENTES = random.uniform(10,30)                #Clientes llegan cada 4 minutos en una distribucion uniforme
TIEMPO_TRABAJO = 28800                                  #8 horas el dia en segundos, ya que el tiempo de servicio de los algoritmos puede ser en segundos
UTILIDAD = 0                                            #Utilidad total del estudiante al terminar el juego
UTILIDAD_PROFES = 0                                     #utilidad del profesor, esta solo crece cuando el profe gana, si pierde no decrece
UTILIDAD_WIN = 10                                       #utilidad del estudiante cuando el profesor gana es la mitad de la ganancia total, es 15
UTILIDAD_LOSE = -5                                     	#utilidad cuando el profesor pierde, solo se le quita al estudiante la utilidad ganada
GANADAS=0                                               #nos almacena el numero de partidas ganadas por el profesor
PERDIDAS=0                                              #nos almacena el numero de partidas perdidas por el profesor
JUGADAS=0                                               #nos almacena el numero de partidas jugadas
COLA = 0                                                #nos almacena el tamaño de la cola en el momento(se usa para uno de los escenarios)
MAX_COLA = 0                                            #nos almacena el tamaño de la cola maxima (se usa para uno de los escenarios)
ESPERA_CLIENTES = np.array([])                          #nos almacena en un arreglo el tiempo de espera (se usa para uno de los escenarios)
TIEMPO_SERVICIO_PROFE =  np.array([])                   #nos almacena en un arreglo con el tiempo de servicio del prefesor jugando, hacemos uso de
                                                        #la funcion numpy para el arreglo 
TIEMPO_SERVICIO_ROBOT = np.array([])                    #nos almacena en un arreglo con el tiempo de servicio del robot jugando, hacemos uso de la
                                                        #funcion numpy para el arreglo 
TABLEROS=[4,6,8,10,12,15]                               #posible tamaño del tablero para jugar, son seleccionados por los robots 

#Funcion llegada:       esta funcion nos simula la llegada de los clientes, los cuales
#                       llegan de acuerdo a una distribucion exponencial con media 4 
#                       minutos por cada cliente.
def llegada(env, serv):
        global JUGADAS
        while True:
                JUGADAS +=1                             #aqui llevamos la cuenta de las partidas jugadas
                c = cliente(env,'Robot uribito3000_desgraciado #%02d' % JUGADAS, serv)
                                                        #iniciamos el proceso de recibir el robot 
                env.process(c)                          #iniciamos la simulacion del ambiente con el robot que ha llegado
                tiempo_llegada = LLEGADA_CLIENTES
                yield env.timeout(tiempo_llegada)       #Yield retorna un objeto iterable
                if (env.now>TIEMPO_TRABAJO):            #el ambiente estara activo hasta que se cumpla el TIEMPO_TRABAJO 
                        break
#Funcion cliente:       simula la atencion de un robot que ha llegado a retar al profesor                                                
def cliente(env, nombre, servidor):                     #El robot llega y se va cuando pierde o gana    
    llegada = env.now                                   #momento presente en la simulacion 
    print('%7.2f'%(llegada)," Llega el ",nombre ," a retar al profesor Carlitos y Rusvelt")
    global UTILIDAD
    global UTILIDAD_PROFES                               #se definen globales las variables definidas arribas para poder usarlas en toda la simulacion
    global GANADAS
    global PERDIDAS    
    global TIEMPO_SERVICIO_PROFE
    global TIEMPO_SERVICIO_ROBOT
    global COLA
    global MAX_COLA 
    global ESPERA_CLIENTES
                                                        #Atendemos a los robots (retorno del yield)
    with servidor.request() as req:                     #With ejecuta un iterador sin importar si hay excepciones o no
		
        COLA += 1
        if COLA > MAX_COLA:
                MAX_COLA = COLA

        results = yield req

        COLA = COLA - 1
        espera = env.now - llegada
        ESPERA_CLIENTES = np.append(ESPERA_CLIENTES, espera)        
        print('%7.2f'%(llegada), " Se inicia la partida con el ",nombre)

        opcion_robot = random.choice(TABLEROS)          #el robot escoge un tamaño del tablero
        print('el robot ha escogido un tablero de tamaño : ',opcion_robot)
        servicioProfesor= 0                             #se inicializan las variables del tiempo de servicio de ambos
        servicioRobot= 0                                #las cuales se iran concatenando con el fin de promedir el tiempo 
                                                        #de servicio de c/u
        print('\n')                                                        
        llegadaProfe =time.time()                       #se captura el tiempo de inicio del profesor

        if ((JUGADAS%2)==0):
        	print('Carlitos en modo sabio de los seis caminos juega:')
        if ((JUGADAS/2)!=0):
        	print('Rusvelt en modo Dios matematico juega:')

        sm.solucionNReinas(opcion_robot)                #el profesor juega con su algoritmo determinista
        salidaProfe =time.time()                        #se captura el tiempo en que termina el profesor
        servicioProfesor =salidaProfe - llegadaProfe    #se determina su tiempo de servicio
        TIEMPO_SERVICIO_PROFE = np.append(TIEMPO_SERVICIO_PROFE, servicioProfesor)
                                                        #se concatena en el arreglo para luego determinar su media
        print('\n')
        llegadaRobot =time.time()                       #se captura el tiempo de inicio del profesor
        print('robot uribito3000_desgraciado juega:')
        sm.MasterVegas(opcion_robot)                    #el profesor juega con su algoritmo determinista
        salidaRobot =time.time()                        #se captura el tiempo en que termina el profesor
        servicioRobot =salidaRobot - llegadaRobot       #se determina su tiempo de servicio
        TIEMPO_SERVICIO_ROBOT = np.append(TIEMPO_SERVICIO_ROBOT, servicioRobot)
                                                        #se concatena en el arreglo para luego determinar su media
        print('\n')

        if(servicioProfesor < servicioRobot):           #se validacuando el profesor gana
            print('El robot ha perdido (*tira el tablero hace berrinche por 2 segundos :c)')
            print('*mata a los testigos...')
            print('\n')
            UTILIDAD_PROFES += (UTILIDAD_WIN*2)            #la utilidad del prefesor se incrementa
            UTILIDAD += UTILIDAD_WIN                    #la utilidad del estudiante se incrementa
            GANADAS += 1                                #se lleva la cuenta de las ganadas
            yield env.timeout(servicioProfesor)         

        else:
        	if ((JUGADAS%2)==0):
        		print('Los humanos pierden, el profesor Carlitos se lamenta')
        		print('*uribito3000_desgraciado le pone las botas al reves')
	        if ((JUGADAS%2)!=0):
	        	print('Los humanos pierden, el profesor Rusvelt se lamenta')
	        	print('*uribito3000_desgraciado le pone las botas al reves')

        	print('\n')
        	UTILIDAD += UTILIDAD_LOSE                   #la utilidad del estudiante se incrementa
        	PERDIDAS += 1                               #se lleva la cuenta de las ganadas
        	yield env.timeout(servicioRobot)

#                           Inicio de la simulacion

print('Carlitos-Rusvelt VS Maquina')
random.seed(SEMILLA)
env = sp.Environment()    
#                       Inicio del proceso y ejecucion
servidor = sp.Resource(env, capacity=2)
env.process(llegada(env, servidor))
env.run()
#               Impresion de variables al terminar la simulacion
print("Cola maxima:                                     	", MAX_COLA)
print("La utilida  ganada por Carlitos y Rusvelt fue:   	", UTILIDAD_PROFES)
print("La utilida  ganada por el estutiante fue:        	", UTILIDAD)
print("las partidas ganadas por los profesores fueron:  	", GANADAS)
print("las partidas  perdidas por los profesores  fueron:  	", PERDIDAS)
print("Tiempo promedio de servicio de los profesores fue :  ",'%7.15f'%(np.mean(TIEMPO_SERVICIO_PROFE)))
print("Tiempo promedio de servicio del robot fue :   		",'%7.15f'%(np.mean(TIEMPO_SERVICIO_ROBOT)))
print("Numero de partidas jugadas :                     	", JUGADAS)
print("Tiempo promedio de espera  :                  		",'%7.15f'%(np.mean(ESPERA_CLIENTES)))