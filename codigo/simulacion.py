﻿#       Nombre:                 Christian David Ospina Primero
#       Codigo:                 201556123
#       Codigo de programa:     3743
#
#       Nombre:                 Andres Camilo Pulgarin Nopia
#       Codigo:                 201556017
#       Codigo de programa:     3743
#
#       Nombre:                 Kevin Stewart Ramirez Gamba
#       Codigo:                 201556203
#       Codigo de programa:     3743
#---------------------------------------------------------------------------------------
import numpy as np
import random                                               #importacion de modulos
import sys
#---------------------------------------------------------------------------------------------------------------------------------------
#
#                                           INICIO ALGORITMO LAS DETERMINISTA
#
#Función solucion:          pintar la matriz solucion con las reinas posicionadas
def solucionTablero(solucion):
    print(solucion)                                         #nos imprime el tablero con las reinas colocadas
        
#Funcion validacionPos:     se asegura que se pueda colocar una reina
def validacionPos(reina, f, c, n):
                                            
    for i in range (c):                                     #valida fila hacia la izquierda
        if (reina[f][i] == 2):
            return False
                                            
    for i,j in zip(range(f, -1, -1), range (c, -1, -1)):    #Valida diagonal izquiera arriba
        if reina[i][j] == 2 :
            return False
                                            
    for i, j in zip(range(f, n), range(c, -1, -1)):         #Valida diagonal izquiera abajo
        if reina[i][j] == 2:
            return False
    return True

#Funcion  Backtracking:     recursiva para resolver las n reinas
def Backtracking(reina, c ,n):    
    if c >= n:                                              
        return True                                         #Caso final: si las n reinas están puestas retorna True   
    for i in range(n):                                      #Toma esta columna e intenta colocar una reina en todas las filas una por una     
        if (validacionPos(reina, i, c, n)):                 #mira si la reina puede ser colocada en Reinas[i][c]
            reina[i][c] = 2                                 #Pone la reina en Reina[i][c]                                            
            if (Backtracking(reina, c + 1, n) == True):     #Recurre a poner el resto de la reinas
                return True
                                                            #Si poner la reina en el Reinas[i][c] no lleva a una solución,
                                                            #entonces se retira la reina
                                                                      
            reina[i][c]=0                                   #Backtrack

    return False                                            #retorna false indicando que no se han puesto todas las reinas 

#Funcion solucionNReinas:   Esta función resuleve el problema de las n reinas por medio de Backtracking,
#                           usa principalmente Backtracking() para dar solución al problema,
#                           retorna falso si la reina no puede ser colocada en alguna posición,
#                           de ser otro caso, retorna verdadero y pone la reina en forma de 1
def solucionNReinas(n):
    tablero=np.zeros((n,n))                                 #inicializamos una matriz de 0 para pintar las reinas en el tablero,
                                                            #se hace usando la funcion zeros de numpy

    if (Backtracking(tablero, 0, n) == False):              #en tal caso de que al intentar solucionar el problema, lleguemos a un punto
                                                            #en que no podamos poner mas reinas por que se amenazan, retornamos false y 
                                                            #decimos que no encontramos la solucion y terminamos el algoritmo
        print("No haber solución")
        return False

    solucionTablero(tablero)                                #de lo contrario llamamos la funcion solucionTablero(), que le llega como 
                                                            #parametro la matriz de 0, e imprimimos el tablero solucion
    
    return True                                             #retornamos true para terminar el algoritmo

#solucionNReinas(15)
#
#                                           FIN ALGORITMO LAS DETERMINISTA
#
#---------------------------------------------------------------------------------------------------------------------------------------
#
#                                           INICIO ALGORITMO LAS VEGAS
#
#Funcion matrixInicial(n):      inicializa una matriz de ceros de tamaño nxn para el tablero
def matrixInicial(n):
    matrix=np.zeros((n,n))                                  #usamos la funcion zeros de numpy para inicializar la matriz
    return matrix

#Funcion sumaDiag(valor,i,j):   en esta funcion validamos la diagonales para poner una reina sin estar amenazada
def sumaDiag(valor,i,j):
    der=0                                                   #es la sumatoria de las diagonales derechas
    izq=0                                                   #es la sumatoria de las diagonales izquierdas
    aux1=i                                                  #son los subindices para validar la diagonal izquierda
    aux2=j
    while(i<len(valor)-1 and j<len(valor)-1):               #pregunta por la diagonal derecha hasta que llega al final
                                                            #de la matriz y al final se suma

        der=der+valor[i+1][j+1]+valor[i-1][j]               #sumatoria de las diagonales derechas
        i+=1                                                #se incrementa para pasar a la sgte posicion de la diagonal derecha
        j+=1

    while (aux1<len(valor)-1 and aux2>0):                   #pregunta por la diagonal izquierda hasta que llega al final de la matriz
                                                            #y al final se suma
        izq=izq+valor[aux1+1][aux2-1]+valor[i-1][j]         #sumatoria de las diagonales izquierda
        aux1+=1                                             #se incrementa para pasar a la sgte posicion de la diagonal izquierda
        aux2-=1
    return der+izq                                          #retorna la sumatoria de las diagonales para saber si estan libres las casillas,
                                                            #es decir que no amenaze a ninguna reina, si es igual a 0 no esta siendo atacado,
                                                            #si es mayor a 0 hay una reina en las diagonales

#Funcion colum(matrix,i,j):     validamos la columna pero por debajo de la posicion donde se puso la reina
def colum(matrix,i,j):                                      #verifica si debajo de la reina hay otra, como tal verifica columnas
    colum=0                                                 #almacenamos la sumatoria de las casillas debajo de la reina
    while(i<len(matrix)-1):                                 #itera en el vector columna mientras sea del tamaño de la matriz
        colum=matrix[i+1][j]+colum                          #sumatoria de la columna de las posiciones abjo de la reina
        i=i+1                                               #incrementamos el contador
    return colum                                            #retornamos la sumatoria de la columna


#Funcion vectorPos(n):          inicializa la lista de posibles posiciones para elegir la primera reina que tiene todas las casillas libres
def vectorPos(n):
    vect=[]
    for i in range(n-1):                                    #lleno el vector con las posiciones de 0 a n-1
        vect.append(i)
    return vect                                             #retornamos el vector con las posisciones como valor desde 0 hasta n-1

#Funcion MasterVegas(valor):    pone las reinas desde la ultima fila hacia arriba, validando diagonales y columnas, 
#                               las filas no es necesaria ya que se valida con el while ya que nunca la va a poner 
#                               en la misma fila debido a que el while se incrementa
def MasterVegas(valor):
    matriz=matrixInicial(valor)                             #inicializamos la matriz o tablero
    aux3=valor-1                                            #se usa para ir incrementando las filas
    lis=vectorPos(valor)                                    #inicializamos el vector de las posiciones factibles a poner la reina
    stop = False;                                           #condicion de parada

    while (not stop):
        if not(len(lis)==0):                                #mientras la lista de soluciones factibles no este vacia hace el random
            factibles=random.choice(lis)                    #escogemos una de las posiciones factibles usando la funcion choice de random
        else:
            if(aux3>-1):                                    #si se acaban las soluciones factibles sin haber terminado el tablero 
                                                            #reiniciamosla matriz
                matriz=matrixInicial(valor)                 #llamamos la funcion que nos inicializa la matriz con ceros
                aux3 = valor-1                              #como se reinicia el algoritmo decimos que vale n-1 para que no se sobrepase
            else:
                break
        lis=[]                                              #inicializa la lista de factibles en ceros ya que no se necesitan los valores
                                                            #del anterior

        if sumaDiag(matriz,aux3,factibles)==0 and colum(matriz,aux3,factibles)==0 :
                                                            #preguntamos si las diagonales y columnas estan libres
            matriz[aux3][factibles]=matriz[aux3][factibles]+2
                                                            #de ser asi ponemos una reina 

            for i in range(valor):                          #llena un vector con posibles soluciones solo si tiene las diagonales y la 
                                                            #columna libre

                if (sumaDiag(matriz,aux3-1,i)==0) and colum(matriz,aux3,i)==0 and i!=factibles: 
                                                            #Colum verifica si debajo de la posible solucion
                                                                                 
                    lis.append(i)                           #hay una reina sino la hay lo agraga en la lista de posibles posiciones
            aux3-=1                                         #cabiamos a la sgte fila
                
    solucionTablero(matriz)                                 #pintamos la solucion
    return matriz                                           #salimos retornando la matriz

#MasterVegas(15)